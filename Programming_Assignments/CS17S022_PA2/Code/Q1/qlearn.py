#
# coding: utf-8

# In[ ]:


import gridworlds



import gym
import itertools
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

num_trials = 50

if "../" not in sys.path:
    sys.path.append("../") 

from collections import defaultdict

# In[ ]:

#Choose the variant 

#env = gym.make('PuddleWorldC-v0')
#env = gym.make('PuddleWorldB-v0')
env = gym.make('PuddleWorldA-v0')
#render to show the variant to be worked upon
env.render()


# In[ ]:



def make_epsilon_greedy_policy(Q, epsilon, nA):
    def policy_fn(observation):
        A = np.ones(nA, dtype=float) * epsilon / nA
        best_action = np.argmax(Q[observation])
        A[best_action] += (1.0 - epsilon)
        return A
    return policy_fn


# In[ ]:


def q_learning(env, num_episodes, discount_factor=0.9, alpha=0.5, epsilon=0.1):
 
    
    # The final action-value function.
    # A nested dictionary that maps state -> (action -> action-value).
    Q = defaultdict(lambda: np.zeros(env.action_space.n))

    #steps and reward to be used in plotting later
    steps = [0]*num_episodes  
    rew = [0]*num_episodes 
    
    
    policy = make_epsilon_greedy_policy(Q, epsilon, env.action_space.n)
    
    for i_episode in range(num_episodes):
       
        if (i_episode + 1) % 10 == 0:
            print("\rEpisode {}/{}.".format(i_episode + 1, num_episodes))
            sys.stdout.flush()
        
        # Reset the environment and pick the first action
        state = env.reset()
        print("State after reset: {0}".format(state))

        for t in itertools.count():
            
            # A step
            action_probs = policy(state)
            action = np.random.choice(np.arange(len(action_probs)), p=action_probs)
            next_state, reward, done, _ = env.step(action)

            # Update steps and rew

            steps[i_episode] = t
            rew[i_episode] += reward 
            
            # TD Update
            best_next_action = np.argmax(Q[next_state])    
            td_target = reward + discount_factor * Q[next_state][best_next_action]
            td_delta = td_target - Q[state][action]
            Q[state][action] += alpha * td_delta
                
            if done:
                print("Episode finished after {} timesteps".format(t+1))
                break
                
            state = next_state
    
    return Q, steps, rew


# In[ ]:
traj = 500
sum_steps = [0] * traj
sum_rewards = [0.0]* traj
fins = np.asarray(sum_steps)
finr = np.asarray(sum_rewards)

for i in range (num_trials):
    Q, steps, rew = q_learning(env, traj)
    stepsarr = np.asarray(steps)
    rewarr = np.asarray(rew)
    fins += stepsarr 
    finr += rewarr

fins /= num_trials
finr /= num_trials

plt.figure(figsize=(10,5))
plt.title("Average number of steps to goal over 50 independent trials | Goal C")
plt.xlabel("Episode")
plt.ylabel("Average number of steps to goal")
plt.grid(True)
plt.plot(fins)
#plt.savefig('C_steps.png')
plt.show()

plt.figure(figsize=(10,5))
plt.title("Average reward per episode over 50 independent trials | Goal C")
plt.xlabel("Episode")
plt.ylabel("Average rewards per episode")
plt.grid(True)
plt.axis("auto")
plt.plot(finr)
#plt.savefig('C_rewards.png')
plt.show()





