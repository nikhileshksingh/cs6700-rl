
# coding: utf-8

# In[19]:


import gym
import gridworlds
import itertools

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

if "../" not in sys.path:
  sys.path.append("../") 

from collections import defaultdict



# In[20]:

num_trials = 50
#Choose variant
#env = gym.make('PuddleWorldC-v0')
#env = gym.make('PuddleWorldB-v0')
env = gym.make('PuddleWorldA-v0')



# In[21]:


def make_epsilon_greedy_policy(Q, epsilon, nA):
    def policy_fn(observation):
        A = np.ones(nA, dtype=float) * epsilon / nA
        best_action = np.argmax(Q[observation])
        A[best_action] += (1.0 - epsilon)
        return A
    return policy_fn


# In[22]:


def sarsa(env, num_episodes, discount_factor=1.0, alpha=0.5, epsilon=0.1):

    
    
    # A nested dictionary that maps state -> (action -> action-value) this is for SARSA
    Q = defaultdict(lambda: np.zeros(env.action_space.n))
    

    steps = [0]*num_episodes  
    rew = [0]*num_episodes 


    policy = make_epsilon_greedy_policy(Q, epsilon, env.action_space.n)
    
    for i_episode in range(num_episodes):
        
        if (i_episode + 1) % 100 == 0:
            print("\rEpisode {}/{}.".format(i_episode + 1, num_episodes))
            sys.stdout.flush()
        
        # Reset the environment 
        state = env.reset()
        action_probs = policy(state)
        action = np.random.choice(np.arange(len(action_probs)), p=action_probs)
        
       
        for t in itertools.count():
            # One step
            next_state, reward, done, _ = env.step(action)
            
            # Next action
            next_action_probs = policy(next_state)
            next_action = np.random.choice(np.arange(len(next_action_probs)), p=next_action_probs)
            
            # Update steps and rew
            steps[i_episode] = t
            rew[i_episode] += reward 
            
            # TD Update
            td_target = reward + discount_factor * Q[next_state][next_action]
            td_delta = td_target - Q[state][action]
            Q[state][action] += alpha * td_delta
    
            if done:
                break
                
            action = next_action
            state = next_state        
    
    return Q, steps, rew


# In[23]:
traj = 500
sum_steps = [0] * traj
sum_rewards = [0.0]* traj
fins = np.asarray(sum_steps)
finr = np.asarray(sum_rewards)

for i in range (num_trials):
    Q, steps, rew = sarsa(env, traj)
    stepsarr = np.asarray(steps)
    rewarr = np.asarray(rew)
    fins += stepsarr 
    finr += rewarr

fins /= num_trials #averaging out
finr /= num_trials

plt.figure(figsize=(10,5))
plt.title("Average number of steps to goal over 50 independent trials | Goal A")
plt.xlabel("Episode")
plt.ylabel("Average number of steps to goal")
plt.grid(True)
plt.plot(fins)
#plt.savefig('A_steps_SA.png')
plt.show()

plt.figure(figsize=(10,5))
plt.title("Average reward per episode over 50 independent trials | Goal A")
plt.xlabel("Episode")
plt.ylabel("Average rewards per episode")
plt.grid(True)
plt.axis("auto")
plt.plot(finr)
#plt.savefig('A_rewards_SA.png')
plt.show()

# In[24]:




