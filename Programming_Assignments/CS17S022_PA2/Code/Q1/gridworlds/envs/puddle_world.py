import numpy as np
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from Tkinter import *

import pickle,os

master = Tk()
Width = 50
(x, y) = (12, 12)

#Canvas for the grid
board = Canvas(master, width=x*Width, height=y*Width)
board.pack()


def categorical_sample(prob_n, np_random):
    prob_n = np.asarray(prob_n)
    csprob_n = np.cumsum(prob_n)
    return (csprob_n > np_random.rand()).argmax()


UP = 0
RIGHT = 1
DOWN = 2
LEFT = 3

WORLD_FREE = 0
WORLD_OBSTACLE = 1
WORLD_MINE = 2
WORLD_GOAL = 3
WORLD_PUDDLE = [4, 5, 6]  # Puddle Codes
puddle_rewards = [-1,-2,-3] # Puddle penalties -1, -2, and -3
puddle_dict = {i:j for i,j in zip(WORLD_PUDDLE,puddle_rewards)}

class PuddleWorld(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self, noise=0.0, terminal_reward=10, 
            border_reward=0.0, step_reward=-0.2, start_state_ind=None, wind = 0.5, confusion = 0.1, start_states = None,world_file_path = None,init_map = None):

        def load_map(self, fileName):
            theFile = open(fileName, "rb")
            self.map = np.array(pickle.load(theFile))
            self.n = self.map.shape[0]
            print self.map
            theFile.close()
        # Load a map if no init map provided
        if(init_map is None):
            assert(world_file_path is not None)
            if world_file_path is not None:
                if not os.path.exists(world_file_path):
                    # Now search the saved_maps folder
                    dir_path = os.path.dirname(os.path.abspath(__file__))
                    rel_path = os.path.join(dir_path, "saved_maps", world_file_path)
                    if os.path.exists(rel_path):
                        world_file_path = rel_path
                    else:
                        raise FileExistsError("Cannot find %s." % world_file_path)
                load_map(self,world_file_path)
                print("\nFound Saved Map\n")
        else:
            self.map = init_map
            self.n = self.map.shape[0] # assuming Square shape


        

        self.tile_ids = {WORLD_FREE:step_reward,WORLD_GOAL:terminal_reward,\
         }
        self.tile_ids.update(puddle_dict)

        

        # self.n = n # Uncomment when not loading map
        self.noise = noise
        self.confusion = confusion
        self.terminal_reward = terminal_reward
        self.border_reward = border_reward
        
        self.step_reward = step_reward
        self.n_states = self.n ** 2 + 1
        self.terminal_state = None

        self.set_term_state() # searches map and sets terminal states

        # self.terminal_state = self.n_states - 2 - terminal_state_offset
        self.absorbing_state = self.n_states - 1
        self.done = False

        self.set_start_state(start_states, start_state_ind)        

        # Simulation related variables
        self._reset()
        self._seed()

        self.action_space = spaces.Discrete(4)
        # self.observation_space = spaces.Box(low=np.zeros(2), high=np.zeros(2)+n-1) # use wrapper instead
        self.observation_space = spaces.Discrete(self.n_states) # with absorbing state
        #self._seed()

    def set_term_state(self):
        # searches map and sets terminal states
        goal_locs = np.where(self.map == WORLD_GOAL)
        goal_coords = np.c_[goal_locs]
        self.term_states = [self.coord2ind(c) for c in goal_coords] # allows multiple goal states

        if (len(self.term_states)>0): self.terminal_state = self.term_states[0] # Picking first one
        else: self.terminal_state = -1
        assert(self.terminal_state is not None)
        print("Terminal state {0} and location : {1}".format(self.term_states,self.terminal_state))

    def set_start_state(self, start_states = None, start_state_ind = None):
        self.start_state_ind = start_state_ind
        if start_states is None:
            self.start_states = [[6, 1], [7, 1], [11, 1], [12, 1]]
        elif start_states ==[]: # random start states hack
            candidate_starts = np.where(self.map != WORLD_OBSTACLE)
            start_coords = np.c_[candidate_starts]
            self.start_states = [c for c in start_coords] # picks ALL states apart from obstacles
        else:
            self.start_states = start_states

    def _step(self, action):
        assert self.action_space.contains(action)

        if self.state == self.terminal_state:
            self.state = self.absorbing_state #reset env first
            self.done = True
            return self.state, self._get_reward(), self.done, None

        [row, col] = self.ind2coord(self.state)
        print("s : {0}, ({1},{2})".format(self.state,row,col))
        if np.random.rand() < self.noise: # Randomly pick an action
            action = self.action_space.sample()
        
        if(np.random.rand() < self.confusion):  # if confused, choose others with probability 0.1/3
            rand_act = self.action_space.sample()
            while rand_act == action: # while action is the same as picked, keep sampling
                rand_act = self.action_space.sample()
            action = rand_act

        if action == UP:
            row = max(row - 1, 0)
        elif action == DOWN:
            row = min(row + 1, self.n - 1)
        elif action == RIGHT:
            col = min(col + 1, self.n - 1)
        elif action == LEFT:
            col = max(col - 1, 0)

        new_state = self.coord2ind([row, col])
        print("s_new : {0},({1},{2})".format(new_state,row,col))
        # Check if new state is an obstacle
        if(self.map.T.take(new_state) == WORLD_OBSTACLE):
            new_state = self.state # State remains unchanged

        reward = self._get_reward(new_state=new_state)

        self.state = new_state

        # Set self.done at end of step
        if self.state == self.terminal_state or self.state in self.term_states:
            self.done = True
            return self.state, self._get_reward(), self.done, None

        return self.state, reward, self.done, None




    def _get_reward(self, new_state=None):
        if self.done:
            return self.terminal_reward

        tile = self.map.T.take(new_state)
        reward = self.tile_ids[tile] # Use the reward dictionary to give reward based on tile

        r,c = self.ind2coord(new_state)

        return reward



    def at_border(self):
        [row, col] = self.ind2coord(self.state)
        return (row == 0 or row == self.n - 1 or col == 0 or col == self.n - 1)

    def ind2coord(self, index):
        assert(index >= 0)
        #assert(index < self.n_states - 1)

        col = index // self.n
        row = index % self.n

        return [row, col]


    def coord2ind(self, coord):
        [row, col] = coord
        assert(row < self.n)
        assert(col < self.n)

        return col * self.n + row


    def _reset(self):
        if(self.start_state_ind is None): # i.e. if start state is not fixed
            start_state_ind = np.random.randint(len(self.start_states))
        else:
            start_state_ind = self.start_state_ind
        # print(self.start_states,start_state_ind)
        self.start_state = self.coord2ind(self.start_states[start_state_ind])
        self.state = self.start_state #if not isinstance(self.start_state, str) else np.random.randint(self.n_states - 1)
        self.done = False
        return self.state

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _render(self, mode='human', close=False):
        #render function
        #global agent 
        #() = self.ind2coord(self.state)
        #print agent
        specials=[]
        wall1=[]
        wall2=[] 
        wall3=[] 
        boundary =[]
        Width = 50 
        x =12
        y =12

        #specials = np.where((self.map == WORLD_GOAL)

        ls = (np.c_[np.where(self.map == WORLD_GOAL)])
        specials = []
        for i in ls:
            specials.append(tuple(i))

        ls = (np.c_[np.where(self.map == WORLD_PUDDLE[0])])

        for i in ls:
            wall3.append(tuple(i))    

         
        ls = (np.c_[np.where(self.map == WORLD_PUDDLE[1])])
        for i in ls:
            wall2.append(tuple(i))  


        
        ls = (np.c_[np.where(self.map == WORLD_PUDDLE[2])])
        for i in ls:
            wall1.append(tuple(i))   

        ls = (np.c_[np.where(self.map == WORLD_OBSTACLE)])
        for i in ls:
            boundary.append(tuple(i))               
   

        start = [(6, 1), (7, 1), (11, 1), (12, 1)]

        for i in range(x):
            
            for j in range(y):
                board.create_rectangle(j*Width, i*Width, (j+1)*Width, (i+1)*Width, fill="white", width=1)
                
       
        for (i, j) in specials:
            board.create_rectangle((j-1)*Width, (i-1)*Width, (j)*Width, (i)*Width, fill="green", width=1)
        for (i, j) in wall1:
            board.create_rectangle((j-1)*Width, (i-1)*Width, (j)*Width, (i)*Width, fill="black", width=1)
        for (i, j) in wall2:
            board.create_rectangle((j-1)*Width, (i-1)*Width, (j)*Width, (i)*Width, fill="gray", width=1)
        for (i, j) in wall3:
            board.create_rectangle((j-1)*Width, (i-1)*Width, (j)*Width, (i)*Width, fill="silver", width=1)
        for (i, j) in start:
            board.create_rectangle((j-1)*Width, (i-1)*Width, (j)*Width, (i)*Width, fill="aqua", width=1)
        #ag = board.create_rectangle((j-1)*Width, (i-1)*Width, (j)*Width, (i)*Width, fill="aqua", width=1)
        mainloop()        




class PuddleWorldA(PuddleWorld):

    def __init__(self):
        super(PuddleWorldA, self).__init__(world_file_path="PuddleWorldA.dat")

class PuddleWorldB(PuddleWorld):

    def __init__(self):
        super(PuddleWorldB, self).__init__(world_file_path="PuddleWorldB.dat")

class PuddleWorldC(PuddleWorld):

    def __init__(self):
        super(PuddleWorldC, self).__init__(world_file_path="PuddleWorldC.dat")



