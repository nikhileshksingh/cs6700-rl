
# coding: utf-8

# In[ ]:



import gym
import itertools
import gridworlds

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

if "../" not in sys.path:
    sys.path.append("../") 

from collections import defaultdict

num_trials = 25

# In[ ]:


env = gym.make('PuddleWorldA-v0')


# In[ ]:


# In[ ]:


def make_epsilon_greedy_policy(Q, epsilon, nA):

    def policy_fn(observation):
        A = np.ones(nA, dtype=float) * epsilon / nA
        best_action = np.argmax(Q[observation])
        A[best_action] += (1.0 - epsilon)
        return A
    return policy_fn


# In[ ]:


def sarsa(env, num_episodes, discount_factor=0.9, alpha=0.5, epsilon=0.1,_lambda = 0.3):

    
    
    # A nested dictionary that maps state -> (action -> action-value).
    Q = defaultdict(lambda: np.zeros(env.action_space.n))
    # ELigibilty traces
    E = defaultdict(lambda: np.zeros(env.action_space.n))
    


    steps = [0]*num_episodes  
    rew = [0]*num_episodes

 
    policy = make_epsilon_greedy_policy(Q, epsilon, env.action_space.n)
    
    for i_episode in range(num_episodes):

        if (i_episode + 1) % 100 == 0:
            print("\rEpisode {}/{}.".format(i_episode + 1, num_episodes))
            sys.stdout.flush()
        
        # Reset the environment
        state = env.reset()
        #env.render()
        print("State after reset: {0}".format(state))
        action_probs = policy(state)
        action = np.random.choice(np.arange(len(action_probs)), p=action_probs)
        
        
        for t in itertools.count():
            # Take a step
            next_state, reward, done, _ = env.step(action)
            print("Next State: {0}".format(next_state))
            # Pick the next action
            next_action_probs = policy(next_state)
            next_action = np.random.choice(np.arange(len(next_action_probs)), p=next_action_probs)
            
            # Update steps and rew
            steps[i_episode] = t
            rew[i_episode] += reward 
            
            # TD Update
            td_target = reward + discount_factor * Q[next_state][next_action]
            td_delta = td_target - Q[state][action]
            E[state][action] = E[state][action] + 1
            
            #Update elgibilty traces
            for s in range(env.observation_space.n - 1):
                for a in range(env.action_space.n):
                    Q[s][a] += alpha * td_delta * E[s][a]
                    E[s][a] = _lambda * discount_factor * E[s][a]
    
            if done:
                print("Episode finished after {} timesteps".format(t+1))
                break
                
            action = next_action
            state = next_state        
    
    return Q, steps, rew

# In[ ]:
traj = 500
sum_steps = [0] * traj
sum_rewards = [0.0]* traj
fins = np.asarray(sum_steps)
finr = np.asarray(sum_rewards)


lambdas = [0,0.3,0.5,0.9,0.99,1.0]


plt.figure(figsize=(10,5))

for j in lambdas:
    for i in range (num_trials):
    	Q, steps, rew = sarsa(env, traj, _lambda = j)
    	stepsarr = np.asarray(steps)
    	#rewarr = np.asarray(rew)
    	fins += stepsarr 
    	#finr += rewarr

    fins /= num_trials
    #finr /= num_trials
    



    plt.plot(fins, label= '$\lambda$ = '+str(j))



plt.title("Average number of steps to goal for different value of $\lambda$ | Goal A")
plt.xlabel("Episode")
plt.ylabel("Average number of steps to goal over all trials")
plt.grid(True)
plt.legend(loc='best')
#plt.savefig('sarlambsteps.png')
plt.show()



plt.figure(figsize=(10,5))

for j in lambdas:
    for i in range (num_trials):
    	Q, steps, rew = sarsa(env, traj, _lambda = j)
    	#stepsarr = np.asarray(steps)
    	rewarr = np.asarray(rew)
    	#fins += stepsarr 
    	finr += rewarr

    #fins /= num_trials
    finr /= num_trials
    



    plt.plot(finr, label= '$\lambda$ = '+str(j))



plt.title("Average reward per episode for different values of $\lambda$| Goal A")
plt.xlabel("Episode")
plt.ylabel("Average reward per episode over all trials")
plt.grid(True)
plt.legend(loc='best')
#plt.savefig('sarlambrew.png')
plt.show()



