from collections import defaultdict
import numpy as np
import gym
import click


def get_logp_action(theta, ob, action):

    ob_1 = include_bias(ob)
    mean = theta.dot(ob_1)
    zs = action - mean
    return -0.5 * np.log(2 * np.pi) * theta.shape[0] - 0.5 * np.sum(np.square(zs))

def get_grad_logp_action(theta, ob, action):
    ob_1 = include_bias(ob)
    grad = np.outer(action - np.dot(theta, ob_1), ob_1)
    return grad

def point_get_action(theta, ob, rng=np.random):
    ob_1 = include_bias(ob)
    mean = theta.dot(ob_1)
    return rng.normal(loc=mean, scale=1.)
