
# coding: utf-8

# In[1]:


import pandas as pd
import glob as glob
import numpy as np
import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')


# In[2]:


print(plt.style.available)
styles = plt.style.available
style = styles[10]
plt.style.use(style)


# In[4]:


df1 = pd.read_csv('best_model.csv', header=None, sep=' ')


# In[5]:


#df2 = pd.read_csv('', header=None, sep=' ')


# In[6]:


#df3 = pd.read_csv('', header=None, sep=' ')


# In[7]:


#df4 = pd.read_csv('', header=None, sep=' ')


# In[12]:


x_axis = df1[0][:41]
y_axis_1 = df1[1][:len(x_axis)]
# y_axis_2 = df2[1][:len(x_axis)]
# y_axis_3 = df3[1][:len(x_axis)]
# y_axis_4 = df4[1][:len(x_axis)]
x_label = 'Number of Episodes'
y_label = 'Avg Reward'

label1 = 'Average reward'
# label2 = 'epsilon = 0.01'
# label3 = 'epsilon = 0.001'
# label4 = 'epsilon = 1'
filename= 'best.png'


# In[13]:


colors = [ 'red', 'cyan', 'yellow','orange', 'magenta', 'violet', 'purple']

fig = plt.figure(figsize=(15,10))

plt.plot(x_axis, y_axis_1 , color=colors[0], label=label1, alpha=0.75)
#plt.scatter(x_axis, y_axis_1, label=None, c=color1, alpha=0.4)

# plt.plot(x_axis, y_axis_2 , color=color2, label=label2, alpha=0.75)
# # #plt.scatter(df2['Step'], y_axis_2,s=110, c=color2, alpha=0.75, label='Best Bleu Score')

# plt.plot(x_axis, y_axis_3 , color=color3, label=label3, alpha=0.75)

# plt.plot(x_axis, y_axis_4 , color=color4, label=label4, alpha=0.75)

plt.xlabel(x_label)
plt.ylabel(y_label)
#plt.legend(loc='best', frameon=True, prop={'size' : 12}) # prop is to set the font properties of the legend

#plt.title(title)

plt.savefig(filename, bbox_inches='tight',transparent=False,pad_inches=.2, dpi = 200)
plt.show()

